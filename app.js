

var bugetController= (function (){
	var Expense = function(id,description,value){
		this.id=id;
		this.description=description;
		this.value=value;
		this.percentage = -1;
	};

	Expense.prototype.calcutePercentage = function(totalInc){
		if(totalInc > 0)
			this.percentage = Math.round((this.value / totalInc)* 100);
		else
			this.percentage = -1;
	};

	var Income = function(id,description,value){
		this.id=id;
		this.description=description;
		this.value=value;
	};

	var calcuteTotal = function(type){
		var sum=0;
		data.allItems[type].forEach(function(current,index,array){
			sum += current.value;
		});
		data.totals[type]=sum;
	};	
	

	var data = {
		allItems: {
			exp : [],
			inc :[]
		},
		totals : {
			exp:0,
			inc: 0
		},
		budget : 0,
		percentage: -1,

	};

	return{
		addItem : function(type,des,val){
			var newItem,ID;
			//create new ID
			if(data.allItems[type].length > 0)
				ID= data.allItems[type][data.allItems[type].length -1].id + 1 ;
			else
				ID = 0;

			if(type==='exp'){
				newItem = new Expense(ID,des,val);
			}else if(type==='inc'){
				newItem = new Income(ID,des,val);
			}
			data.allItems[type].push(newItem);

			return newItem;
		},
		calcuteBudget : function(){
			calcuteTotal('exp');
			calcuteTotal('inc');
			data.budget = data.totals.inc - data.totals.exp;
			if(data.totals.inc > 0)
				data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
			else
				data.percentage = -1;

		},
		calcutePercentages : function(){
			data.allItems.exp.forEach(function(current,index,array){
				current.calcutePercentage(data.totals.inc);
			});
		},
		getPercentages : function(){
			var percentages = data.allItems.exp.map(function(current,index,array){
				return current.percentage;
			});
			return percentages;
		},
		getBudget: function(){
			return{
				budget : data.budget,
				totalInc: data.totals.inc,
				totalExp: data.totals.exp,
				percentage: data.percentage,
			};
		},
		deleteItem : function(type,id){
			var ids,index;
			ids = data.allItems[type].map(function(current,index,array){
				return current.id;
			});
			index = ids.indexOf(id);
			if(index >= 0){
				data.allItems[type].splice(index,1);
			}
		},
		testing : function(){
			console.log(data);
		}
	};

})();



var UIController=(function(){
	var DOMStrings={
		inputType: '.add__type',
		inputDescription: '.add__description',
		inputValue: '.add__value',
		inputButton: '.add__btn',
		incomeContainer: '.income__list',
		expensesContainer: '.expenses__list',
		budgetLabel: '.budget__value',
		incomeLabel: '.budget__income--value',
		expensesLabel: '.budget__expenses--value',
		percentageLabel: '.budget__expenses--percentage',
		container: '.container',
		exensesPercLabel: '.item__percentage',
		dateLable: '.budget__title--month'
	};
	var nodeListsForEach = function(list,callback){
		for(var i =0 ; i < list.length ; i++){
			callback(list[i],i,list);
		}
	};

	return {
		getInput : function(){
			return {
				type:document.querySelector(DOMStrings.inputType).value,
				description: document.querySelector(DOMStrings.inputDescription).value,
				value: parseFloat(document.querySelector(DOMStrings.inputValue).value),
			};
		},
		getDOMStrings: function(){
			return DOMStrings;
		},
		addListItem: function(obj,type){
			var html,newhtml,element;
			//create HTML String
			if(type === "inc"){
				element=DOMStrings.incomeContainer;
				html= 
				'<div class="item clearfix" id="inc-%id%">\
				<div class="item__description">%description%</div>\
				<div class="right clearfix">\
				<div class="item__value">+ %value%</div>\
				<div class="item__delete">\
				<button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\
				</div>\
				</div>\
				</div>';
			}else if(type === 'exp'){
				element=DOMStrings.expensesContainer;
				html= 
				'<div class="item clearfix" id="exp-%id%">\
				<div class="item__description">%description%</div>\
				<div class="right clearfix">\
				<div class="item__value">- %value%</div>\
				<div class="item__percentage">21%</div>\
				<div class="item__delete">\
				<button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button>\
				</div>\
				</div>\
				</div>';
			}
			//replace the placeholders
			newhtml = html.replace('%id%',obj.id);
			newhtml = newhtml.replace('%description%',obj.description);
			newhtml = newhtml.replace('%value%',this.formatNumber(obj.value));
			//insert using DOM
			document.querySelector(element).insertAdjacentHTML('beforeend',newhtml);

		},
		deleteListItem : function(selectorId){
			var item= document.getElementById(selectorId);
			item.parentNode.removeChild(item);
		},
		clearFields: function(){
			var flieds,fliedsArr;
			flieds= document.querySelectorAll(DOMStrings.inputDescription + ", "+ DOMStrings.inputValue);
			fliedsArr= Array.prototype.slice.call(flieds);
			fliedsArr.forEach(function(current,index,array) {
				current.value="";
			});
			fliedsArr[0].focus();
		},
		displayBudget: function(obj){
			document.querySelector(DOMStrings.budgetLabel).innerHTML = this.formatNumber(obj.budget);
			document.querySelector(DOMStrings.incomeLabel).innerHTML = "+" + this.formatNumber(obj.totalInc);
			document.querySelector(DOMStrings.expensesLabel).innerHTML = "-" + this.formatNumber(obj.totalExp);
			if(obj.percentage > 0)
				document.querySelector(DOMStrings.percentageLabel).innerHTML = obj.percentage + "%";	
			else
				document.querySelector(DOMStrings.percentageLabel).innerHTML = "---";	
		},
		displayPercentages: function(percentages){
			var items = document.querySelectorAll(DOMStrings.exensesPercLabel);

			nodeListsForEach(items,function(current,index){
				if(percentages[index] > 0)
					current.innerHTML = percentages[index] + "%";
				else 
					current.innerHTML = "--";

			});

		},
		formatNumber : function(number){
			var numsplit,int,dec,strNum;
			number = number.toFixed(2);
			numsplit = number.split('.');
			int = numsplit[0];
			dec = numsplit[1];
			if(int.length > 3){
				strNum="";
				var count =0;
				for (var i = int.length - 1; i >= 0 ; i--) {
					count++;
					strNum= int.charAt(i)+ strNum;
					if(i == 0 || (number < 0 && i == 1))
						continue;
					if(count==3 ){
						strNum= ","+ strNum;
						count=0;
					}
				}
				int = strNum;
			}
			return int +"."+ dec;
		},
		displayMonth : function(){
			var now,year,month;
			now= new Date();
			year = now.getFullYear();
			month = now.getMonth();
			document.querySelector(DOMStrings.dateLable).innerHTML=  (month+1) + "-" + year;
		},
		changeType : function(){
			var flieds = document.querySelectorAll(DOMStrings.inputType+","+DOMStrings.inputDescription
				+","+DOMStrings.inputValue);
			nodeListsForEach(flieds,function(current){
				current.classList.toggle('red-focus');
			});
			document.querySelector(DOMStrings.inputButton).classList.toggle('red');
		},
	};
})();


var controller=(function(budgetCtrl,UICtrl){

	var setupEventListeners= function(){
		var DOMStrings = UICtrl.getDOMStrings();
		document.querySelector(DOMStrings.inputButton).addEventListener('click',ctrlAddItem);
		document.addEventListener('keypress',function(event){
			if(event.keyCode === 13 || event.which === 13){
				ctrlAddItem();
			}
		});
		document.querySelector(DOMStrings.container).addEventListener('click',ctrlDeleteItem);
		document.querySelector(DOMStrings.inputType).addEventListener('change', UICtrl.changeType);
	};
	var ctrlAddItem= function(){
		var input,newItem;
		//get the input data 
		input = UICtrl.getInput();
		if(input.description !== "" && !isNaN(input.value) && input.value > 0){
		//add the item to the buget controller
		newItem= budgetCtrl.addItem(input.type,input.description,input.value);
		//add item to the UI
		UICtrl.addListItem(newItem,input.type);
		UICtrl.clearFields();
		//update budget
		updateBudget();
		//update precentages
		updatePercentages();
	}	
	
};

var ctrlDeleteItem = function(event){
	var itemID,type,id;
	///retrive the id
	itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;
	if(itemID){
		splitID= itemID.split('-');
		type = splitID[0];
		id = parseInt(splitID[1]);
		//delete item from data 
		budgetCtrl.deleteItem(type,id);
		//delete item from UI
		UICtrl.deleteListItem(itemID);
		//update buget (on UI)
		updateBudget();
		//update precentages
		updatePercentages();
	}

};

var updatePercentages = function(){
	//caculate percentages
	budgetCtrl.calcutePercentages();
		//read from budget controller

		var percentage= budgetCtrl.getPercentages();
		UICtrl.displayPercentages(percentage);
	//update the UI
};

var updateBudget= function(){
	var budget;
		//calcute the budget 
		budgetCtrl.calcuteBudget();
		//return the budget
		budget= budgetCtrl.getBudget();
		//display the budget to UI
		UICtrl.displayBudget(budget);
	};

	return {
		init : function(){
			UICtrl.displayBudget({
				budget : 0,
				totalInc: 0,
				totalExp: 0,
				percentage: 0,
			});
			UICtrl.displayMonth();
			setupEventListeners();
			console.log("App Has Started");

		}
	};

})(bugetController,UIController);

controller.init();

